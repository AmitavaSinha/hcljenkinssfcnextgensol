﻿CREATE TABLE [dbo].[OrderHeader] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [OrderDate]       DATE            NULL,
    [OrderByUserName] NVARCHAR (100)  NULL,
    [OrderByUserID]   NVARCHAR (100)  NULL,
    [TotalAmount]     DECIMAL (18, 2) NULL,
    [Status]          NVARCHAR (50)   NULL,
    [ShippingAddress] NVARCHAR (200)  NULL,
    [PaymentMethod]   NVARCHAR (50)   NULL,
    CONSTRAINT [PK_OrderHeader] PRIMARY KEY CLUSTERED ([ID] ASC)
);

