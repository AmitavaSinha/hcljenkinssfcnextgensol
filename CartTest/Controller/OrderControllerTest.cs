﻿using Cart.MVC.Controllers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CartTest.Controller
{
    [TestFixture]
    public class OrderControllerTest
    {
        [Test]
        public void MyOrders()
        {
            var controller = new OrderController();
            var result = controller.MyOrders() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        [Test]
        public void OrderDetails()
        {
            var controller = new OrderController();
            var result = controller.OrderDetails(30) as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        [Test]
        public void GetAllOrders()
        {
            var controller = new OrderController();
            var result = controller.MyOrders() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }

    }
}
