﻿CREATE TABLE [dbo].[Products] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [ProductCategory]    NVARCHAR (200)  NULL,
    [ProductTitle]       NVARCHAR (MAX)  NULL,
    [ProductDescription] NVARCHAR (MAX)  NULL,
    [ProductImage]       NVARCHAR (200)  NULL,
    [ProductPrice]       DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([ID] ASC)
);

