﻿using Cart.MVC.Controllers;
using NUnit.Framework;
using System.Web.Mvc;

//Changed in CartTest Controller
namespace CartTest.Controller
{
    
    [TestFixture]
    public class CartControllerTest
    {
        [Test]
        public void CartCheckOut()
        {
            var controller = new CartController();
            var result = controller.CheckOut() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        //This is Cart Confirm Order
        [Test]
        public void CartConfirmOrder()
        {
            var controller = new CartController();
            var result = controller.CheckOut() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }

        [Test]
        public void GetOrderDetails()
        {
            var controller = new CartController();
            var result = controller.CheckOut() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
    }
}
