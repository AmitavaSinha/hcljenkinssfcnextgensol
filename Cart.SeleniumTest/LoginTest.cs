﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Cart.SeleniumTest
{

    [TestClass]
    public class LoginTest
    {
        string _baseURL = string.Empty;
        RemoteWebDriver _driver = null;
        public TestContext TestContext { get; set; }

        //Changed by Amitava Sinha
        private RemoteWebDriver GetChromeDriver()
        {
            var path = Environment.GetEnvironmentVariable("ChromeWebDriver");
          // var path = @"C:\Selinium Driver";
            var options = new ChromeOptions();
            options.AddArguments("--no-sandbox");

            if (!string.IsNullOrWhiteSpace(path))
            {
                return new ChromeDriver(path, options, TimeSpan.FromSeconds(300));
            }
            else
            {
                return new ChromeDriver(options);
            }
        }

        [TestInitialize]
        public void Setup()
        {
            this._baseURL = @"https://hclshoppingcart.azurewebsites.net/";
            _driver = GetChromeDriver();
        }

        [TestMethod]
        public void ClickLoginButtonWithoutUserNamePassword()
        {
            this._driver.Navigate().GoToUrl(this._baseURL);
            this._driver.FindElement(By.Id("UserName")).SendKeys(string.Empty);
            this._driver.FindElement(By.Id("Password")).SendKeys(string.Empty);
            this._driver.FindElement(By.XPath(".//*[@value='Login']")).Click();
            string userNameErrorText = this._driver.FindElement(By.XPath(".//span[@data-valmsg-for='UserName']")).Text;
            string passwordErrorText = this._driver.FindElement(By.XPath(".//span[@data-valmsg-for='Password']")).Text;
            Assert.AreEqual("The User Name field is required.", userNameErrorText);
            Assert.AreEqual("The Password field is required.", passwordErrorText);
            PushScreenshot();
        }
        [TestMethod]
        public void ClickLoginButtonWithPassword()
        {
            this._driver.Navigate().GoToUrl(this._baseURL);
            this._driver.FindElement(By.Id("UserName")).SendKeys("amitava.sinha@hcl.com");
            this._driver.FindElement(By.Id("Password")).SendKeys(string.Empty);
            this._driver.FindElement(By.XPath(".//*[@value='Login']")).Click();
            string passwordErrorText = this._driver.FindElement(By.XPath(".//span[@data-valmsg-for='Password']")).Text;
            Assert.AreEqual("The Password field is required.", passwordErrorText);
            PushScreenshot();
        }
        [TestMethod]
        public void ClickLoginButtonWithoutUserName()
        {
            this._driver.Navigate().GoToUrl(this._baseURL);
            this._driver.FindElement(By.Id("UserName")).SendKeys(string.Empty);
            this._driver.FindElement(By.Id("Password")).SendKeys("Password");
            this._driver.FindElement(By.XPath(".//*[@value='Login']")).Click();
            string userNameErrorText = this._driver.FindElement(By.XPath(".//span[@data-valmsg-for='UserName']")).Text;
            Assert.AreEqual("The User Name field is required.", userNameErrorText);
            PushScreenshot();
        }
        [TestMethod]
        public void UserNameAphaNumericValidation()
        {
            this._driver.Navigate().GoToUrl(this._baseURL);
            this._driver.FindElement(By.Id("UserName")).SendKeys("amitava.sinha123@hcl.com");
            this._driver.FindElement(By.Id("Password")).SendKeys(string.Empty);
            this._driver.FindElement(By.XPath(".//*[@value='Login']")).Click();
            IWebElement element = this._driver.FindElement(By.Id("UserName"));
            string userNameErrorText = element.GetAttribute("value");
            string uName = userNameErrorText.Split('@')[0];
            bool isTrue = IsAlphaNumeric(uName);
            Assert.AreEqual(true, isTrue);
            PushScreenshot();
        }
        [TestMethod]
        public void UserEmailValidation()
        {
            this._driver.Navigate().GoToUrl(this._baseURL);
            this._driver.FindElement(By.Id("UserName")).SendKeys("amitava.sinha123@hcl.com");
            this._driver.FindElement(By.Id("Password")).SendKeys(string.Empty);
            this._driver.FindElement(By.XPath(".//*[@value='Login']")).Click();
            IWebElement element = this._driver.FindElement(By.Id("UserName"));
            string userNameEmailText = element.GetAttribute("value");
            bool isTrue = IsValidEmail(userNameEmailText);
            Assert.AreEqual(true, isTrue);
            PushScreenshot();
        }

        [TestMethod]
        public void PasswordLengthValidation()
        {
            this._driver.Navigate().GoToUrl(this._baseURL);
            this._driver.FindElement(By.Id("UserName")).SendKeys("amitava.sinha@hcl.com");
            this._driver.FindElement(By.Id("Password")).SendKeys("india");
            this._driver.FindElement(By.XPath(".//*[@value='Login']")).Click();
            IWebElement element = this._driver.FindElement(By.Id("Password"));
            string userPasswordLength = element.GetAttribute("value");
            int passwordlen = userPasswordLength.Length;
            if (string.IsNullOrEmpty(userPasswordLength))
                Assert.AreEqual("0", passwordlen.ToString());
            PushScreenshot();
        }

        public void PushScreenshot()
        {
            var filePath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString()) + ".png";
            var screenshot = this._driver.GetScreenshot();
            screenshot.SaveAsFile(filePath);
            TestContext.AddResultFile(filePath);
        }

        [TestCleanup]
        public void TearDown()
        {
            this._driver.Close();
            this._driver.Dispose();
        }
        public bool IsAlphaNumeric(String strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex(@"^[0-9.]+$");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }

        bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }
    }
}



