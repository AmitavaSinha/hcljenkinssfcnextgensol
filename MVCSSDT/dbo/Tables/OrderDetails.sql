﻿CREATE TABLE [dbo].[OrderDetails] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [OrderHeaderID] INT             NULL,
    [ProductID]     INT             NULL,
    [Quantity]      INT             NULL,
    [Rate]          DECIMAL (18, 2) NULL,
    [Amount]        DECIMAL (18, 2) NULL,
    [Status]        NVARCHAR (50)   NULL,
    CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_OrderDetails_OrderHeader] FOREIGN KEY ([OrderHeaderID]) REFERENCES [dbo].[OrderHeader] ([ID]),
    CONSTRAINT [FK_OrderDetails_Products] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Products] ([ID])
);

