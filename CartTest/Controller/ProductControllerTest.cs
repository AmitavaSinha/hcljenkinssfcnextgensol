﻿using Cart.MVC.Controllers;
using Cart.MVC.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CartTest.Controller
{
    [TestFixture]
    public class ProductControllerTest
    {
        [Test]
        public void ProductIndex()
        {
            var controller = new ProductController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        [Test]
        public void ProductSearch()
        {
            var controller = new ProductController();
            var result = controller.SearchProduct("Camera") as ViewResult;            
            Assert.AreEqual("Index",result.ViewName);
        }

        [Test]
        public void ProductAll()
        {
            var controller = new ProductController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
    }
}
