﻿using Cart.MVC.Controllers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CartTest.Controller
{
    [TestFixture]
    public class HomeControllerTest
    {
        [Test]
        public void Index()
        {
           // Assert.AreEqual(1, 1);
            // Arrange
           HomeController controller = new HomeController();
            //// Act
           ViewResult result = controller.Index() as ViewResult;
            // Assert
           Assert.IsNotNull(result);
        }

        [Test]
        public void About()
        {
           // Assert.AreEqual(1, 1);
            // Arrange
            HomeController controller = new HomeController();
            //// Act
            ViewResult result = controller.About() as ViewResult;
            //// Assert
            Assert.AreEqual("Shopping Cart Application.", result.ViewBag.Message);
        }

        [Test]
        public void Contact()
        {
           // Assert.AreEqual(1, 1);
            // Arrange
            HomeController controller = new HomeController();
            //// Act
            ViewResult result = controller.Contact() as ViewResult;
            //// Assert
            Assert.IsNotNull(result);
        }
        public void Login()
        {
            // Assert.AreEqual(1, 1);
            // Arrange
            HomeController controller = new HomeController();
            //// Act
            ViewResult result = controller.Index() as ViewResult;
            // Assert
            Assert.IsNotNull(result);
        }
    }
}
